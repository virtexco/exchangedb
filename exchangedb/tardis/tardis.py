import requests
import pandas as pd
import sqlalchemy
from sqlalchemy import create_engine
import logging
from dotenv import load_dotenv
import os

logging.basicConfig(level=logging.INFO)
load_dotenv()  # take environment variables from .env.
instruments = True

logging.info('Opening Database Connection')
conn_string = os.environ.get('EXCHANGEDB', 'postgresql://postgres:postgres@localhost/exchangedb')
db = create_engine(conn_string)
conn = db.connect()

logging.info('Loading Exchange Info')
r = requests.get('https://api.tardis.dev/v1/exchanges')
tardis = pd.DataFrame(r.json())
print(tardis)
logging.info("Writing Exchange Info to ExchangeDB")
tardis.to_sql('tardis', db, index=False, if_exists='replace')
logging.info("Done")
# exit()

if(instruments):
    logging.info('Loading Instrument Info')
    df_instruments = pd.DataFrame()
    for venue in tardis.id: #[tardis.id=='deribit']:
        logging.info("Loading Venue: {venue}".format(venue=venue))
        url = 'https://api.tardis.dev/v1/instruments/{venue}'.format(venue=venue)
        r_instruments = requests.get(url)
        df_instruments = df_instruments.append(r_instruments.json())
    print(df_instruments)
    logging.info('Writing to database')
    df_instruments.to_sql('tardis_instruments', db, index=False, if_exists='replace', dtype={'changes': sqlalchemy.types.JSON})
