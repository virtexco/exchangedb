import time
import pandas as pd
import sqlalchemy
from sqlalchemy import create_engine
import psycopg2
import pycoingecko
from pycoingecko import CoinGeckoAPI
import logging
from dotenv import load_dotenv
import os
import requests


load_dotenv()  # take environment variables from .env.
logging.basicConfig(level=logging.INFO)
cg = CoinGeckoAPI()


def get_exchanges():
    # Load Exchanges from CoinGecko
    logging.info('Getting exchange data from Coingecko API')
    results = pd.DataFrame()
    page = 1
    per_page = 250
    while True:
        _results = cg.get_exchanges_list(per_page=per_page, page=page)
        results = results.append(_results)
        page = page + 1
        if len(_results) < per_page:
            break

    logging.debug(results)
    return results


def get_coins():
    logging.info('Getting coin data from Coingecko API')
    results = pd.DataFrame()
    page = 1
    per_page = 250
    max_pages = -1
    while True:
        logging.info('Getting page {}'.format(page))
        _results = cg.get_coins_markets(vs_currency='usd', sparkline=False, price_change_percentage='24h', per_page=per_page, page=page)
        results = results.append(_results)
        page = page + 1
        if len(_results) < per_page or (page > max_pages and max_pages > 0):
            break

    logging.debug(results)
    return results


def get_coin_tickers(coin):
    logging.info('Getting tickers for {coin}'.format(coin=coin))
    results = pd.DataFrame()
    page = 1
    per_page = 100
    max_pages = -1
    while True:
        logging.info('Getting {coin} page {page}'.format(coin=coin, page=page))
        _results = cg.get_coin_ticker_by_id(coin, include_exchange_logo=False, order='volume_desc', depth=False, page=page)
        results = results.append(_results['tickers'])
        page = page + 1
        if len(_results['tickers']) < per_page or (page > max_pages and max_pages > 0):
            break

    logging.debug(results)
    return results


def get_coins_tickers(coins):
    logging.info('Getting coin tickers for {coins}'.format(coins=coins))
    results = pd.DataFrame()
    for coin in coins:
        results = results.append(get_coin_tickers(coin))

    logging.info('Reshaping Data')
    results['venue'] = pd.json_normalize(results.market).identifier
    results['last_usd'] = pd.json_normalize(results.converted_last).usd
    results['volume_usd'] = pd.json_normalize(results.converted_volume).usd
    results.drop(['market', 'converted_last', 'converted_volume'], axis=1, inplace=True)

    return results


def init_db():
    logging.info('Initializing connection to ExchangeDB')
    conn_string = os.environ.get('EXCHANGEDB', 'postgresql://postgres:postgres@localhost/exchangedb')
    db = create_engine(conn_string)
    conn = db.connect()
    return conn


def get_markets():
    return pd.read_json('https://api.coingecko.com/api/v3/coints/list')


def get_prices(coin_name, symbol, days=30):
    price_array = requests.get('https://api.coingecko.com/api/v3/coins/{coin_name}/market_chart?vs_currency=usd&days={days}&interval=daily'.format(coin_name=coin_name, days=days))
    prices = pd.DataFrame(price_array.json()['prices'], columns=['timestamp', 'price'])
    prices['timestamp'] = pd.to_datetime(prices.timestamp, unit='ms')
    prices['symbol'] = symbol
    return prices


def get_all_prices(markets):
    df = pd.DataFrame()
    for id, row in markets.iterrows():
        df = pd.concat([df, get_prices(row.id, row.symbol)], ignore_index=True)
    return df    


if __name__ == '__main__':
    db = init_db()
    # get_exchanges().to_sql('coingecko', db, index=False, if_exists='replace')
    
    # coins = get_coins()
    # coins.to_sql('coins', db, index=False, if_exists='replace', dtype={'roi': sqlalchemy.types.JSON})

    coin_tickers = get_coins_tickers(['bitcoin','tether'])
    coin_tickers.to_sql('pairs', db, index=False, if_exists='replace')

    logging.info('Done')
