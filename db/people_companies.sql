create table people_companies (
    person int NOT NULL,
    company int NOT NULL,
    status int NOT NULL,
    primary key (person, company),
    CONSTRAINT fk_person FOREIGN KEY(person) REFERENCES people(id),
    CONSTRAINT fk_company FOREIGN KEY(company) REFERENCES companies(id)
)