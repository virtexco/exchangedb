create table companies (
    id int generated always as identity primary key,
    entityname text,
    entitytype text,
    website text,
    notes text
)