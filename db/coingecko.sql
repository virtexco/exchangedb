create table coingecko (
    id varchar not null primary KEY,
    name varchar not null,
    year_established int,
    country VARCHAR,
    description TEXT,
    url varchar,
    image varchar,
    has_trading_incentive boolean,
    trust_score int,
    trust_score_rank int,
    trade_volume_24h_btc DOUBLE,
    trade_volume_24h_btc_normalized DOUBLE,
);