create table venues (
    id int generated always as identity primary key,
    name varchar not null unique,
    mic varchar unique,
    website varchar,
    api_url varchar,
    fees json,
    
);