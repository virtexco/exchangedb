create table people (
    id int generated always as identity primary key,
    firstname text,
    lastname text,
    contacts json,
    notes text
)